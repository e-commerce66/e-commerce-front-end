export const categories = [
  {
    id: 1,
    img: "https://res.cloudinary.com/sarahb1/image/upload/v1658768992/ECommerce/LeatherBg_iwexa1_o3pmjn.jpg",
    title: "LEATHER BAGS",
    cat: "Leather",
  },

  {
    id: 2,
    img: "https://res.cloudinary.com/sarahb1/image/upload/v1658768993/ECommerce/NylonBg_krposr_frdujq.jpg",
    title: "NYLON BAGS",
    cat: "Nylon",
  },

  {
    id: 3,
    img: "https://res.cloudinary.com/sarahb1/image/upload/v1658768994/ECommerce/FiberBg_ld3we1_yvu31g.webp",
    title: "FABRIC BAGS",
    cat: "Fabric",
  },
];

// export const popularProducts = [
//   {
//     id: 1,
//     img: "https://images.pexels.com/photos/1102219/pexels-photo-1102219.jpeg",
//     cat: "summer",
//   },
//   {
//     id: 2,
//     img: "https://images.pexels.com/photos/6310184/pexels-photo-6310184.jpeg",
//     cat: "winter",
//   },
//   {
//     id: 3,
//     img: "https://images.unsplash.com/photo-1591561954555-607968c989ab?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1792&q=80",
//     cat: "leather",
//   },
//   {
//     id: 4,
//     img: "https://images.unsplash.com/photo-1548036328-c9fa89d128fa?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1738&q=80",
//     cat: "leather",
//   },
//   {
//     id: 5,
//     img: "https://images.unsplash.com/photo-1605733513597-a8f8341084e6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1829&q=80",
//     cat: "winter",
//   },
//   {
//     id: 6,
//     img: "https://images.pexels.com/photos/2115260/pexels-photo-2115260.jpeg",
//     cat: "summer",
//   },
//   {
//     id: 7,
//     img: "https://images.pexels.com/photos/1936848/pexels-photo-1936848.jpeg",
//     cat: "winter",
//   },
//   {
//     id: 8,
//     img: "https://images.pexels.com/photos/1460838/pexels-photo-1460838.jpeg",
//     cat: "summer",
//   },
//   {
//     id: 9,
//     img: "https://images.unsplash.com/photo-1566150905458-1bf1fc113f0d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80",
//     cat: "summer",
//   },
// ];
