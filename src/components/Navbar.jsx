import { Badge, Select, MenuItem } from "@material-ui/core";
import { Search, ShoppingCartOutlined } from "@material-ui/icons";
import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { mobile } from "../responsive";
import { useSelector } from "react-redux";

const Navbar = () => {
  const quantity = useSelector((state) => state.cart?.products?.length);
  const user = useSelector((state) => state.user.currentUser);

  return (
    <Container>
      <Wrapper>
        <Left>
          <Language>
            <Select
              defaultValue="EN"
              IconComponent={() => <></>}
              style={{
                fontSize: "14px",
              }}
            >
              <MenuItem
                value="EN"
                style={{
                  fontSize: "14px",
                }}
              >
                EN
              </MenuItem>
              <MenuItem
                value="KR"
                style={{
                  fontSize: "14px",
                }}
              >
                KR
              </MenuItem>
              <MenuItem
                value="FR"
                style={{
                  fontSize: "14px",
                }}
              >
                FR
              </MenuItem>
            </Select>
          </Language>
          <SearchContainer>
            <Input placeholder="Search" />
            <Link to={`/search`}>
              <Search style={{ color: "gray", fontSize: 16 }} />
            </Link>
          </SearchContainer>
        </Left>
        <Center>
          <Logo>
            <Link to={`/`} style={{ textDecoration: "none", color: "black" }}>
              SARA: H
            </Link>
          </Logo>
        </Center>
        <Right>
          {user && user._id ? (
            <>
              {user && user.isAdmin ? (
                <>
                  <NavItem
                    as={Link}
                    to="/admin"
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    ADMIN
                  </NavItem>
                  <NavItem
                    as={Link}
                    to="/adminorders"
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    ALL ORDERS
                  </NavItem>
                  <NavItem
                    as={Link}
                    to="/logout"
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    LOGOUT
                  </NavItem>
                </>
              ) : (
                <>
                  <NavItem
                    as={Link}
                    to="/logout"
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    LOGOUT
                  </NavItem>
                  <NavItem
                    as={Link}
                    to="/orders"
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    ORDER HISTORY
                  </NavItem>
                </>
              )}
            </>
          ) : (
            <>
              <NavItem
                as={Link}
                to="/register"
                style={{ textDecoration: "none", color: "black" }}
              >
                REGISTER
              </NavItem>
              <NavItem
                as={Link}
                to="/login"
                style={{ textDecoration: "none", color: "black" }}
              >
                SIGN IN
              </NavItem>
            </>
          )}

          <NavItem>
            <Link
              to={`${user && user._id && !user.isAdmin ? "/cart" : "/"}`}
              style={{
                textDecoration: "none",
                color: "black",
                cursor: `${
                  user && user._id && !user.isAdmin ? "pointer" : "not-allowed"
                }`,
              }}
            >
              <Badge
                badgeContent={quantity}
                color="primary"
                overlap="rectangular"
              >
                <ShoppingCartOutlined />
              </Badge>
            </Link>
          </NavItem>
        </Right>
      </Wrapper>
    </Container>
  );
};

export default Navbar;

const Container = styled.div`
  height: 70px;
  ${mobile({ height: "50px" })}

  .MuiInput-underline:before {
    border-bottom: none;
  }
`;
const Wrapper = styled.div`
  padding: 10px 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  ${mobile({ padding: "10px 0px" })}
`;

const Left = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
`;

const Language = styled.span`
  margin-top: 6px;
  font-size: 14px;

  ${mobile({ display: "none" })}
`;
const SearchContainer = styled.div`
  border: 0.5px solid lightgray;
  display: flex;
  align-items: center;
  margin-left: 25px;
  margin-top: 6px;
  padding: 5px;
  border-radius: 8px;
`;

const Input = styled.input`
  border: none;
  ${mobile({ width: "45px" })}
`;

const Center = styled.div`
  flex: 1;
  text-align: center;
`;

const Logo = styled.h1`
  font-weight: bold;
  ${mobile({ fontSize: "20px", marginLeft: "20px" })}
`;
const Right = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  ${mobile({ justifyContent: "center", flex: "1.5" })}
`;

const NavItem = styled.div`
  font-size: 14px;
  cursor: pointer;
  margin-left: 25px;
  ${mobile({ fontSize: "10px", marginLeft: "10px" })}
`;
