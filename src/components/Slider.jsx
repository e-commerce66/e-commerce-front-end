import styled from "styled-components";
import { ArrowLeftOutlined, ArrowRightOutlined } from "@material-ui/icons";
import { useState } from "react";
// import img from "./assets/bag1.jpg";
// import video1 from "./assets/v1.mp4";
// import video2 from "./assets/v2.mp4";
import { mobile, tablet } from "../responsive";

const Container = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  ${mobile({ display: "none" })}
  ${tablet({ height: "50vh" })}
`;

const Arrow = styled.div`
  width: 50px;
  height: 50px;
  background-color: #fff7f7;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  bottom: 0;
  left: ${(props) => props.direction === "left" && "10px"};
  right: ${(props) => props.direction === "right" && "10px"};
  margin: auto;
  cursor: pointer;
  opacity: 0.6;
  z-index: 2;
`;

const Wrapper = styled.div`
  height: 100%;
  display: flex;
  transition: all 1.4s ease;
  transform: translateX(${(props) => props.slideIndex * -100}vw);
`;

const Slide = styled.div`
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #${(props) => props.bg};
`;

const ImgContainer = styled.div`
  height: 100%;
  flex: 0.8;
`;
const Image = styled.img`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
`;

const Video = styled.video`
  height: 100%;
  width: 100%;
`;

const InfoContainer = styled.div`
  flex: 1.2;
  padding: 30px;
`;

const InfoContainer2 = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  z-index: 2;
  background-color: rgba(255, 255, 255, 0.4);
  padding: 30px;
`;

const InfoContainer3 = styled.div`
  flex: 1.2;
  padding: 30px;
  margin-left: 100px;
`;

const ImgContainer2 = styled.div`
  position: relative;
  width: 100%;
`;

const Title = styled.h1`
  font-size: 70px;
  font-family: "Libre Baskerville", serif;
  ${tablet({ fontSize: "40px" })}
`;
const Description = styled.p`
  margin: 50px 0px;
  font-size: 20px;
  font-weight: 500;
  letter-spacing: 3px;
  max-width: 700px;
  ${tablet({ fontSize: "15px" })}
`;
const Button = styled.button`
  padding: 10px;
  font-size: 17px;
  cursor: pointer;
  border: 0.5px solid;
  background-color: transparent;
`;

const Slider = () => {
  const [slideIndex, setSlideIndex] = useState(0);
  const handleClick = (direction) => {
    if (direction === "left") {
      setSlideIndex(slideIndex > 0 ? slideIndex - 1 : 2);
    } else {
      setSlideIndex(slideIndex < 2 ? slideIndex + 1 : 0);
    }
  };
  return (
    <Container>
      <Arrow direction="left" onClick={() => handleClick("left")}>
        <ArrowLeftOutlined />
      </Arrow>
      <Wrapper slideIndex={slideIndex}>
        <Slide bg="f3f7e1">
          <ImgContainer2>
            <Video
              src={
                "https://res.cloudinary.com/sarahb1/video/upload/v1658769151/ECommerce/videos/v2_omxn6a_phod9w.mp4"
              }
              autoPlay
              loop
              muted
            />
          </ImgContainer2>
          <InfoContainer2>
            <Title>GET READY FOR SUMMER!</Title>
            <Description>
              ARE YOU SUMMER READY? GRAB THESE NEW ITEMS AT THE LOWEST PRICE.
            </Description>
            <Button>SHOP NOW</Button>
          </InfoContainer2>
        </Slide>
        <Slide bg="e1eff7">
          <InfoContainer3>
            <Title>WINTER SALE</Title>
            <Description>
              DO NOT COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.
            </Description>
            <Button>SHOP NOW</Button>
          </InfoContainer3>
          <ImgContainer>
            <Video
              src={
                "https://res.cloudinary.com/sarahb1/video/upload/v1658769224/ECommerce/videos/v1_yudwab_ahxezi.mp4"
              }
              autoPlay
              loop
              muted
            />
          </ImgContainer>
        </Slide>
        <Slide bg="e1f7f7">
          <ImgContainer>
            <Image
              style={{ objectFit: "cover" }}
              src={
                "https://res.cloudinary.com/sarahb1/image/upload/v1658769280/ECommerce/bag1_gcoie6_qv0mzt.jpg"
              }
            />
          </ImgContainer>
          <InfoContainer>
            <Title>POPULAR ITEMS</Title>
            <Description>
              A WIDE RANGE OF LEATHER BAGS TO SUIT EVERY PERSONALITY.
            </Description>
            <Button>CHECK HERE</Button>
          </InfoContainer>
        </Slide>
      </Wrapper>
      <Arrow direction="right" onClick={() => handleClick("right")}>
        <ArrowRightOutlined />
      </Arrow>
    </Container>
  );
};

export default Slider;
