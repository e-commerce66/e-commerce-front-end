import styled from "styled-components";
import { mobile } from "../responsive";

const Container = styled.div`
  height: 30px;
  background-color: #1c1c1a;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 12px;
  ${mobile({ height: "25px", fontSize: "10px" })}
`;
const Announcement = () => {
  return <Container>FREE SHIPPING on all orders over P5000</Container>;
};

export default Announcement;
