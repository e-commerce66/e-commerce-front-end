import { useEffect, useState } from "react";
import styled from "styled-components";
import ProductItem from "./ProductItem";
import axios from "axios";

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const PopularProducts = ({ cat, filters, sort }) => {
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);

  useEffect(() => {
    const getProducts = async () => {
      try {
        const res = await axios.get(
          cat
            ? `https://aqueous-inlet-78462.herokuapp.com/products?category=${cat}`
            : "https://aqueous-inlet-78462.herokuapp.com/products"
        );
        setProducts(res.data);
      } catch (err) {}
    };
    getProducts();
  }, [cat]);

  useEffect(() => {
    cat &&
      setFilteredProducts(
        products.filter((item) =>
          Object.entries(filters).every(([key, value]) =>
            item[key].includes(value)
          )
        )
      );
  }, [products, cat, filters]);

  return (
    <Container>
      {cat
        ? filteredProducts.map((item) => <ProductItem item={item} key={item} />)
        : products
            .filter((product) => product.inStock === true)
            .slice(1, 10)
            .map((item) => <ProductItem item={item} key={item} />)}
    </Container>
  );
};

export default PopularProducts;
