import styled from "styled-components";
// import { popularProducts } from "../data";
import Product from "./ProductItem";
import { mobile } from "../responsive";
import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

const Container = styled.div`
  margin-bottom: 40px;
  padding: 10;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  ${mobile({ flexDirection: "column", width: "50%" })}
`;
// const Header = styled.h1`
//   text-align: center;
//   margin-top: 30px;
//   margin-bottom: 20px;
//   ${mobile({ fontSize: "20px" })}
// `;

const Products = ({ cat, filters, sort }) => {
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);

  const { category } = useParams();

  // Getting products to database
  useEffect(() => {
    const getProducts = async () => {
      try {
        const res = await axios.get(
          cat
            ? `https://aqueous-inlet-78462.herokuapp.com/products?category=${cat}`
            : "https://aqueous-inlet-78462.herokuapp.com/products"
        );
        const inStockProducts = res.data.filter((product) => product.inStock);
        setProducts(inStockProducts);
      } catch (err) {}
    };
    getProducts();
  }, [cat]);

  // Filtering products
  useEffect(() => {
    cat &&
      setFilteredProducts(
        products.filter((item) =>
          Object.entries(filters).every(([key, value]) =>
            item[key].includes(value)
          )
        )
      );
  }, [products, cat, filters]);

  // Sorting products
  useEffect(() => {
    if (sort === "Highest Price to Lowest Price") {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => b.price - a.price)
      );
    } else {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => a.price - b.price)
      );
    }
  }, [sort]);

  return (
    <div>
      <Container>
        {cat
          ? filteredProducts
              .filter((item) => item.cat === category)
              .map((item) => <Product item={item} key={item.id} />)
          : products
              .slice(0, 6)
              .filter((item) => item.cat === category)
              .map((item) => <Product item={item} key={item.id} />)}
      </Container>
    </div>
  );
};

export default Products;
