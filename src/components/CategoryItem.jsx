import React from "react";
import styled from "styled-components";
import { mobile } from "../responsive";
import { Link } from "react-router-dom";

const Container = styled.div`
  flex: 4;
  height: 83vh;
  max-height: 640px;
  width: 100%;
  position: relative;
  overflow: hidden;
  ${mobile({ height: "10em" })}
`;

const Image = styled.img`
  transition: all 0.5s ease;
  width: 100%;
  height: 100%;
  object-fit: cover;
  ${mobile({ height: "50vh" })}
`;

const Effect = styled.div`
  &: hover ${Image} {
    transform: scale(1.1);
  }
  width: 100%;
`;
const Info = styled.div`
  position: absolute;
  display: flex;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background-color: rgba(120, 122, 122, 0.4);
`;

const Title = styled.h2`
  color: black;
  margin-bottom: 20px;
  ${mobile({ fontSize: "15px" })}
`;
const Button = styled.button`
  border: none;
  padding: 10px;
  background-color: white;
  color: gray;
  cursor: pointer;
  font-weight: 600;
  ${mobile({ fontSize: "10px" })}
`;

const CategoryItem = ({ item }) => {
  // useEffect(() => {
  //   window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  // }, []);
  return (
    <Effect>
      <Container>
        <Link to={`/products/${item.cat}`}>
          <Image src={item.img} />
          <Info>
            <Title>{item.title}</Title>
            <Button>SHOP NOW</Button>
          </Info>
        </Link>
        ;
      </Container>
    </Effect>
  );
};

export default CategoryItem;
