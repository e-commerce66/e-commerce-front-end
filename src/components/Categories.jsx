import React from "react";
import styled from "styled-components";
import { categories } from "../data";
import CategoryItem from "./CategoryItem";
import { mobile } from "../responsive";

const Container = styled.div`
  display: flex;
  padding: 20px 0;
  justify-content: space-between;
  ${mobile({
    flexDirection: "column",
    padding: "0px",
    justifyContent: "center",
  })}
  width: 100%;
`;

const Header = styled.h1`
  text-align: center;
  margin-top: 30px;
  ${mobile({ fontSize: "20px" })}
`;

export const Categories = () => {
  return (
    <div>
      <Header>CATEGORIES</Header>
      <Container>
        {categories.map((item) => (
          <CategoryItem item={item} key={item.id} />
        ))}
      </Container>
    </div>
  );
};
