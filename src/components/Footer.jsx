import styled from "styled-components";
import { Facebook } from "@material-ui/icons";
import { Instagram } from "@material-ui/icons";
import { Twitter } from "@material-ui/icons";
import { Pinterest } from "@material-ui/icons";
import { Room } from "@material-ui/icons";
import { PhoneIphone } from "@material-ui/icons";
import { Email } from "@material-ui/icons";
import { mobile } from "../responsive";

const Container = styled.div`
  display: flex;
  ${mobile({ flexDirection: "column" })}
`;
const Left = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: 20px;
`;
const Logo = styled.h1`
  ${mobile({ fontSize: "1.8em" })}
`;
const Description = styled.p`
  margin: 20px 0px;
  max-width: 400px;
  color: gray;
  ${mobile({ fontSize: "0.8em" })}
`;
const SocialContainer = styled.div`
  display: flex;
`;
const SocialIcon = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 20px;
  background-color: #${(props) => props.color};
  cursor: pointer;
  ${mobile({ width: "30px", height: "30px" })}
`;

const Center = styled.div`
  flex: 1;
  padding: 20px;
  ${mobile({ display: "none" })}
`;

const Title = styled.h3`
  margin-bottom: 30px;
`;
const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  display: flex;
  flex-wrap: wrap;
  color: gray;
`;
const ListItem = styled.li`
  width: 50%;
  margin-bottom: 10px;
`;

const Right = styled.div`
  flex: 1;
  padding: 20px;
  ${mobile({ padding: "16px", backgroundColor: "#fff8f8" })}
`;
const ContactItem = styled.div`
  color: gray;
  display: flex;
  align-items: center;
  margin-bottom: 7px;
  ${mobile({ fontSize: "0.8em", marginBottom: "5px" })}
`;
const Payment = styled.img`
  width: 50%;
`;
const Copyright = styled.div`
  text-align: center;
  color: gray;
  margin-bottom: 15px;
  font-size: 14px;
  ${mobile({ fontSize: "0.7em", backgroundColor: "#fff8f8" })}
`;
const Footer = () => {
  return (
    <>
      <Container>
        <Left>
          <Logo>SARA: H</Logo>
          <Description>
            Founded by the great Sarah in year 2022, SARA: H is widely known for
            providing the best quality of bags in asian countries. But I guess
            it is time for her to wake up.
          </Description>
          <SocialContainer>
            <SocialIcon color="3B5999">
              <Facebook />
            </SocialIcon>
            <SocialIcon color="E4405F">
              <Instagram />
            </SocialIcon>
            <SocialIcon color="55ACEE">
              <Twitter />
            </SocialIcon>
            <SocialIcon color="E60023">
              <Pinterest />
            </SocialIcon>
          </SocialContainer>
        </Left>
        <Center>
          <Title>Useful Links</Title>
          <List>
            <ListItem>Home</ListItem>
            <ListItem>Cart</ListItem>
            <ListItem>Man Bags</ListItem>
            <ListItem>Woman Bags</ListItem>
            <ListItem>Bag Accessories</ListItem>
            <ListItem>My Account</ListItem>
            <ListItem>Wishlist</ListItem>
            <ListItem>Terms</ListItem>
          </List>
        </Center>
        <Right>
          <Title>Contact</Title>
          <ContactItem>
            <Room style={{ marginRight: 10 }} />
            123 North Sacred Heart St. Pasong Putik QC 1118
          </ContactItem>
          <ContactItem>
            <PhoneIphone style={{ marginRight: 10 }} />
            +63 2 4444 9292
          </ContactItem>
          <ContactItem>
            <Email style={{ marginRight: 10 }} />
            sara.h@mail.com
          </ContactItem>
          <Payment src="https://blueriverdental.com/wp-content/uploads/2019/05/creditcards_23.png" />
        </Right>
      </Container>
      <Copyright>
        Copyright &#169; 2022 | Barlis & Ricaro Development | All Rights
        Reserved
      </Copyright>
    </>
  );
};

export default Footer;
