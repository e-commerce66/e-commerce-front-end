import { ShoppingCartOutlined } from "@material-ui/icons";
import { FavoriteBorderOutlined } from "@material-ui/icons";
import { SearchOutlined } from "@material-ui/icons";
import { mobile } from "../responsive";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Stack, Rating } from "@mui/material/";

const ProductItem = ({ item }) => {
  return (
    <ProductContainer>
      <Container>
        <Info>
          <Icon>
            <ShoppingCartOutlined />
          </Icon>
          <Icon>
            <Link
              to={`/product/${item._id}`}
              style={{ textDecoration: "none", color: "black" }}
            >
              <SearchOutlined />
            </Link>
          </Icon>
          <Icon>
            <FavoriteBorderOutlined />
          </Icon>
        </Info>
        <Image src={item.image} />
      </Container>
      <ProductInfo>
        <Title>{item.title}</Title>
        <Stack spacing={2} sx={{ marginBottom: "5px", marginLeft: "-3px" }}>
          <Rating name="size-small" defaultValue={5} size="small" />
        </Stack>
        <Price>${item.price}</Price>
      </ProductInfo>
    </ProductContainer>
  );
};

export default ProductItem;

const Info = styled.div`
  opacity: 0;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.2);
  z-index: 3;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.6s ease;
`;

const ProductContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 400px;
  max-width: 450px;
  height: 350px;
  margin-bottom: 50px;

  ${mobile({ minWidth: "370px", width: "100%" })}
`;

const Container = styled.div`
  position: relative;
  background-color: #e9f5f5;
  flex: 1.5;
  margin: 4px;
  min-width: 400px;
  max-width: 450px;
  min-height: 300px;
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover ${Info} {
    opacity: 1;
  }

  ${mobile({ minWidth: "300px", maxwidth: "350px" })}
`;

const Image = styled.img`
  width: 80%;
  height: 100%;
  object-fit: cover;
  z-index: 2;
`;

const Icon = styled.div`
  cursor: pointer;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background-color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 10px;
  transition: all 0.4s ease;

  &: hover {
    background-color: #3949ab;
    transform: scale(1.1);
  }
`;
const ProductInfo = styled.div`
  margin: 10px 40px;
`;

const Title = styled.h3``;
const Price = styled.p``;
