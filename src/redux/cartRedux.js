import { createSlice } from "@reduxjs/toolkit";

const cartSlice = createSlice({
  name: "cart",
  initialState: {
    products: [],
    quantity: 0,
    total: 0,
  },
  reducers: {
    addProduct: (state, action) => {
      state.quantity += 1;
      state.products.push(action.payload);
      state.total += action.payload.price * action.payload.quantity;
    },
    resetCart: (state, action) => {
      state.quantity = 0;
      state.products = [];
      state.total = 0;
    },
    removeProduct: (state, action) => {
      state.total -= state.products[action.payload.itemIndex].price * state.products[action.payload.itemIndex].quantity
      state.products.splice(action.payload.itemIndex, 1)
    },
    addQuantity: (state, action) => {
      state.products[action.payload.itemIndex].quantity += 1;
      state.products[action.payload.itemIndex].total += state.products[action.payload.itemIndex].total

      state.total += state.products[action.payload.itemIndex].price
    },
    reduceQuantity: (state, action) => {
      if(state.products[action.payload.itemIndex].quantity === 0) return
      state.products[action.payload.itemIndex].quantity -= 1;
      state.products[action.payload.itemIndex].total -= state.products[action.payload.itemIndex].total      

      state.total -= state.products[action.payload.itemIndex].price
    },
  },
});

export const { addProduct, resetCart, removeProduct, addQuantity, reduceQuantity } = cartSlice.actions;
export default cartSlice.reducer;
