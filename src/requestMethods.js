import axios from "axios";

const BASE_URL = "https://aqueous-inlet-78462.herokuapp.com/";

export const publicRequest = axios.create({
  baseURL: BASE_URL,
});

export const userRequest = axios.create({
  baseURL: BASE_URL,
})

export const adminRequest = axios.create({
  baseURL: BASE_URL,
});

// headers: {
//   'Authorization': 'Bearer sk_test_51LFg2cDDWavmENNmTIZJdZ7MWccDYQFoF75V19fTs4DwMe9SShZX6hXviLRTe3kxoxyuaIENspFUlIL5FuR85K8h00QdkSsiNe',
//   'Content-Type': 'application/json'
// }
