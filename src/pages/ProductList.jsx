import styled from "styled-components";
import Announcement from "../components/Announcement";
import Navbar from "../components/Navbar";
import Products from "../components/Products";
import NewsLetter from "../components/NewsLetter";
import Footer from "../components/Footer";
import { mobile, tablet } from "../responsive";
import { useLocation } from "react-router-dom";
import { useState, useEffect } from "react";

const Container = styled.div``;
const Title = styled.h1`
  margin: 20px;
`;
const FilterContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;
const Filter = styled.div`
  margin: 20px;
  ${mobile({ width: "0px 20px", display: "flex", flexDirection: "column" })}
`;

const FilterText = styled.span`
  font-size: 20px;
  font-weight: 600;
  ${mobile({ marginRight: "0px" })}
  ${tablet({ display: "flex" })}
`;

const Select = styled.select`
  padding: 10px;
  margin: 0 10px;
  ${mobile({ margin: "5px 0px", padding: "7px 0px" })}
`;
const Option = styled.option``;

const ProductList = () => {
  const location = useLocation();
  const cat = location.pathname.split("/")[2];
  const [filters, setFilters] = useState({});
  const [sort, setSort] = useState("New Arrival");

  const handleFilters = (e) => {
    const value = e.target.value;
    setFilters({
      ...filters,
      [e.target.name]: value,
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Container>
      <Announcement />
      <Navbar />
      <Title>{cat} Bags</Title>
      <FilterContainer>
        <Filter>
          <FilterText>Filter Products:</FilterText>
          <Select name="type" onChange={handleFilters}>
            <Option disabled selected>
              Type
            </Option>
            <Option>All</Option>
            <Option>Shoulder</Option>
            <Option>Tote</Option>
            <Option>Backpack</Option>
            <Option>Handbag</Option>
            <Option>Crossbody</Option>
            <Option>Sling</Option>
            <Option>Two-way</Option>
            <Option>Men</Option>
          </Select>
          <Select name="color" onChange={handleFilters}>
            <Option disabled selected>
              Color
            </Option>
            <Option>All</Option>
            <Option>Black</Option>
            <Option>White</Option>
            <Option>Brown</Option>
            <Option>Beige</Option>
            <Option>Cream</Option>
            <Option>Blue</Option>
            <Option>Red</Option>
            <Option>Pink</Option>
            <Option>Yellow</Option>
            <Option>Green</Option>
            <Option>Orange</Option>
          </Select>
        </Filter>
        <Filter>
          <FilterText>Sort Products:</FilterText>
          <Select onChange={(e) => setSort(e.target.value)}>
            <Option disabled selected>
              Sort Price
            </Option>
            <Option value="Highest Price to Lowest Price">
              Highest Price to Lowest Price
            </Option>
            <Option value="Lowest Price to Highest Price">
              Lowest Price to Highest Price
            </Option>
          </Select>
        </Filter>
      </FilterContainer>
      <Products cat={cat} filters={filters} sort={sort} />
      <NewsLetter />
      <Footer />
    </Container>
  );
};

export default ProductList;
