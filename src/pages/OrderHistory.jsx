import Announcement from "../components/Announcement";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import styled from "styled-components";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Skeleton } from "@mui/material";
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@material-ui/core";
import { publicRequest } from "../requestMethods";
import { Favorite } from "@material-ui/icons";
import moment from "moment";

const OrderHistory = () => {
  const [orders, setOrders] = useState(null);
  const [loading, setLoading] = useState(false);
  const user = useSelector((state) => state?.user?.currentUser);

  const getAllOrders = async (userId) => {
    setLoading(true);

    try {
      const res = await publicRequest.get(`/orders/find/${userId}`, {
        headers: {
          token: `Bearer ${user?.accessToken}`,
        },
      });
      setOrders(res.data);
      setLoading(false);
    } catch {
      setLoading(false);
    }
  };

  // const getAllOrders = async (userId) => {
  //   setLoading(true);

  //   try {
  //     const res = await adminRequest.get(`/orders`, {
  //       headers: {
  //         token: `Bearer ${user?.accessToken}`,
  //       },
  //     });
  //     setOrders(res.data);
  //     setLoading(false);
  //   } catch {
  //     setLoading(false);
  //   }
  // };

  useEffect(() => {
    getAllOrders(user._id);
  }, [user]);

  const allOrders = loading
    ? [...Array(5)].map((row, index) => (
        <TableRow key={index}>
          <TableCell width="10%">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell width="30%">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell width="40%">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell width="10%">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell width="10%">
            <Skeleton variant="text" />
          </TableCell>
        </TableRow>
      ))
    : orders?.map((order, index) => (
        <TableRow key={`${order._id}${index}`}>
          <TableCell variant="body" width="10%">
            {order.firstName} {order.lastName}
          </TableCell>
          <TableCell variant="body" width="30%">
            {order.address}
          </TableCell>
          <TableCell variant="body" width="40%">
            {order.products.map((product) => (
              <ProductTitle>
                <Favorite />
                {product.quantity}x {product.title}
              </ProductTitle>
            ))}
          </TableCell>
          <TableCell
            variant="body"
            width="10%"
            style={{
              minWidth: "240px",
            }}
          >
            ${order.amount}
          </TableCell>
          <TableCell variant="body" width="10%">
            {moment(order.createdAt).format("MMMM Do YYYY, h:mm:ss a")}
          </TableCell>
        </TableRow>
      ));

  return (
    <>
      <Announcement />
      <Navbar />
      <Wrapper>
        <Title>Order History</Title>
        <Body>
          <Table stickyHeader>
            <TableHead>
              <TableRow>
                <TableCell variant="head">RECIPIENT</TableCell>
                <TableCell variant="head">ADDRESS</TableCell>
                <TableCell variant="head">PRODUCTS</TableCell>
                <TableCell variant="head">AMOUNT</TableCell>
                <TableCell variant="head">ORDERED AT</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{allOrders}</TableBody>
          </Table>
        </Body>
      </Wrapper>
      <Footer />
    </>
  );
};

const Wrapper = styled.h1``;

const Body = styled.div`
  display: flex;
  padding: 16px 5vw 0 5vw;
  overflow: auto;

  margin-bottom: 40px;
`;

const Title = styled.div`
  margin: 20px 0px;
  text-align: center;
`;

const ProductTitle = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  margin-bottom: 8px;

  font-weight: 600;

  > svg {
    margin-right: 4px;

    path {
      fill: #3f51b5;
    }
  }
`;

export default OrderHistory;
