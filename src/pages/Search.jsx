import Announcement from "../components/Announcement";
import Navbar from "../components/Navbar";
import NewsLetter from "../components/NewsLetter";

import Footer from "../components/Footer";
import styled from "styled-components";

const Search = () => {
  return (
    <div>
      <Announcement />
      <Navbar />
      <Body>
        <h2>
          Search Result for
          {/* {localStorage.getItem("search")}: {resultLength}{" "} */}
        </h2>
      </Body>
      <NewsLetter />
      <Footer />
    </div>
  );
};

export default Search;

const Body = styled.div`
  text-align: center;
`;
