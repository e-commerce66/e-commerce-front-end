import { useState } from "react";
import styled from "styled-components";
import { login } from "../redux/apiCalls";
import { mobile, tablet } from "../responsive";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
// import { Snackbar, Alert } from "@mui/material";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  // const { isFetching, error } = useSelector((state) => state.user);

  const handleClick = (e) => {
    login(dispatch, { username, password });
  };

  return (
    <Container>
      <Wrapper>
        <Title>SIGN IN</Title>
        <Form>
          <Input
            placeholder="username"
            onChange={(e) => setUsername(e.target.value)}
            required
          />
          <Input
            placeholder="password"
            type="password"
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <Button onClick={handleClick} disabled={user?.isFetching}>
            LOGIN
          </Button>
          {user.error && <Error>Something went wrong.</Error>}
          <StyledLink>FORGET PASSWORD?</StyledLink>
          <StyledLink as={Link} to="/register">
            CREATE A NEW ACCOUNT
          </StyledLink>
          <h4 style={{ marginTop: "10px" }}>LOGIN INFO FOR USER</h4>
          <p>username: pollybarlis</p>
          <p>password: abc123</p>
          <h4 style={{ marginTop: "6px" }}>LOGIN INFO FOR ADMIN</h4>
          <p>username: adminaccount</p>
          <p>password: abc123</p>
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Login;

const Container = styled.div`
  flex: 2
  width: 100wv;
  height: 100vh;
  background: url('https://res.cloudinary.com/sarahb1/image/upload/v1658769319/ECommerce/bgreg_lqxfum_dojddl.png');
  background-color: #737fc9;
  background-repeat: no-repeat;
  background-size: cover;
  display: flex;
  align-items: center;
  position: relative;
  ${mobile({ justifyContent: "center", margin: "auto" })}
`;
const Wrapper = styled.div`
  padding: 30px;
  width: 30%;
  background-color: rgba(255, 255, 255, 0.5);
  position: absolute;
  margin-left: 100px;
  ${mobile({ width: "70%", marginLeft: "0px" })}
  ${tablet({ width: "80%", marginLeft: "80px" })}
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;
const Input = styled.input`
  min-width: 40%;
  margin: 10px;
  padding: 10px;
`;

const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: #3949ab;
  color: white;
  cursor: pointer;
  margin: 10px 0px;

  &:hover {
    background-color: #4c5cba;
  }
`;

const StyledLink = styled.a`
  margin: 3px 0px;
  font-size: 13px;
  cursor: pointer;
  text-decoration: none;
  color: black;

  &:hover {
    text-decoration: underline;
  }
`;

const Error = styled.span`
  color: red;
`;
