import styled from "styled-components";
import { mobile } from "../responsive";
import { useState } from "react";
import { register } from "../redux/apiCalls";
import { Link } from "react-router-dom";
import { ViewColumn } from "@material-ui/icons";

const Register = () => {
  const [userDetails, setUserDetails] = useState({
    firstName: "",
    lastName: "",
    username: "",
    email: "",
    password: "",
  });

  const [validPassword, setValidPassword] = useState(null);

  const handleClick = (e) => {
    e.preventDefault();

    register(userDetails);
  };

  return (
    <Container>
      <Logo style={{ marginBottom: "30px", fontSize: "50px" }}>
        <Link to={`/`} style={{ textDecoration: "none", color: "black" }}>
          SARA: H
        </Link>
      </Logo>
      <Wrapper>
        <Title>REGISTER</Title>
        <Form>
          <Input
            placeholder="First Name"
            onChange={(e) =>
              setUserDetails((prevState) => ({
                ...prevState,
                firstName: e.target.value,
              }))
            }
          />
          <Input
            placeholder="Last Name"
            onChange={(e) =>
              setUserDetails((prevState) => ({
                ...prevState,
                lastName: e.target.value,
              }))
            }
          />
          <Input
            placeholder="Username"
            onChange={(e) =>
              setUserDetails((prevState) => ({
                ...prevState,
                username: e.target.value,
              }))
            }
          />
          <Input
            type="email"
            placeholder="Email"
            onChange={(e) =>
              setUserDetails((prevState) => ({
                ...prevState,
                email: e.target.value,
              }))
            }
          />
          <Input
            placeholder="Password"
            onChange={(e) =>
              setUserDetails((prevState) => ({
                ...prevState,
                password: e.target.value,
              }))
            }
          />
          <Input
            placeholder="Confirm Password"
            onChange={(e) =>
              userDetails.password === e.target.value
                ? setValidPassword(true)
                : setValidPassword(false)
            }
          />
          {validPassword === false && (
            <span style={{ color: "red", marginTop: "7px" }}>
              Your password does not match!
            </span>
          )}
          <Declaration>
            <Agreement>
              By creating an account, I consent to the processing of my personal
              data in accordance with the <b>PRIVACY POLICY</b>
            </Agreement>
            <Login>
              Already have an account? Login{" "}
              <Link to={`/login`} style={{ color: "black" }}>
                here.
              </Link>
            </Login>
          </Declaration>

          <Button onClick={handleClick}>CREATE</Button>
        </Form>
      </Wrapper>
    </Container>
  );
};

const Container = styled.div`
  width: 100wv;
  height: 100vh;
  background: url(https://img.freepik.com/free-vector/hand-drawn-flat-design-pastel-color-background_23-2149377038.jpg?w=2000);
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;
const Logo = styled.h1``;

const Wrapper = styled.div`
  padding: 20px;
  width: 40%;
  background-color: rgba(255, 255, 255, 0.5);
  ${mobile({ width: "70%" })}
`;
const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;
const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
  ${mobile({ flexDirection: "column" })}
`;
const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 20px 10px 0px 0px;
  padding: 10px;
`;
const Agreement = styled.span`
  font-size: 12px;
  margin: 5px 0px;
`;
const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: #3949ab;
  color: white;
  cursor: pointer;

  &:hover {
    background-color: #4c5cba;
  }
`;

const Declaration = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 12px;
  margin: 10px 0px;
`;

const Login = styled.p``;

export default Register;
