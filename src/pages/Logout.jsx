import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { logout } from "../redux/userRedux";
import { resetCart } from "../redux/cartRedux";

const Logout = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(logout());
    dispatch(resetCart());
    localStorage.clear();
    navigate("/", { replace: true });
  }, []);

  // Redirect back to login
  return <></>;
};

export default Logout;
