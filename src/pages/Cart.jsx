import styled from "styled-components";
import Announcement from "../components/Announcement";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import { Add, Remove, DeleteForever } from "@material-ui/icons";
import { mobile } from "../responsive";
import { useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import { publicRequest } from "../requestMethods";
import {
  Modal,
  Box,
  TextField,
  Fade,
  Button as ModalButton,
  Typography,
  Backdrop,
  Snackbar,
  Alert,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import {
  removeProduct,
  addQuantity,
  reduceQuantity,
  resetCart,
} from "../redux/cartRedux";

const Cart = () => {
  const cart = useSelector((state) => state.cart);
  const user = useSelector((state) => state?.user?.currentUser);
  const dispatch = useDispatch();
  const [product, setProduct] = useState({});

  const [quantity, setQuantity] = useState(1);
  const history = useNavigate();
  const [addOrder, setAddOrder] = useState({
    userId: user?._id,
    firstName: user?.firstName,
    lastName: user?.lastName,
    products: cart?.products?.map((product) => product),
    quantity: cart?.products?.map((product) => product.quantity),
    amount: cart?.total,
    address: "",
  });

  const [loading, setLoading] = useState(true);
  const [showAddProductDrawer, setShowAddProductDrawer] = useState(false);
  const [triggerRefetch, setTriggerRefetch] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showError, setShowError] = useState({
    show: false,
    message: "",
  });

  // For modal
  const [open, setOpen] = useState(false);

  const handleOrderCreate = async (order) => {
    //merges products from redux to local state orders
    const mergeOrder = {
      ...order,
      products: cart.products?.map((product) => product),
    };

    if (order.products.length < 1)
      return setShowError({
        show: true,
        message: "Please add a product to your cart first.",
      });
    if (!order.address)
      return setShowError({
        show: true,
        message: "Please input an address.",
      });
    if (!order.firstName)
      return setShowError({
        show: true,
        message: "Please input your first name.",
      });
    if (!order.lastName)
      return setShowError({
        show: true,
        message: "Please input your last name.",
      });

    setLoading(true);
    try {
      const res = await publicRequest.post(
        `/orders/`,
        {
          ...mergeOrder,
        },
        {
          headers: {
            token: `Bearer ${user?.accessToken}`,
          },
        }
      );

      setShowAddProductDrawer(false);
      setTriggerRefetch(true);
      setShowSuccess(true);
      setLoading(false);
      dispatch(resetCart());
      setOpen(false);
    } catch (err) {
      setShowError({
        show: true,
        message: `Unable create order due to ${err}!`,
      });
    }
  };

  // Adding or removing item
  const handleQuantity = (type) => {
    if (type === "dec") {
      quantity > 1 && setQuantity(quantity - 1);
    } else {
      setQuantity(quantity + 1);
    }
  };

  const addProductQuantity = (index) => {
    dispatch(addQuantity({ itemIndex: index }));
  };

  const reduceProductQuantity = (index) => {
    dispatch(reduceQuantity({ itemIndex: index }));
  };

  const handleDeleteProductInCart = (productIndex) => {
    dispatch(removeProduct({ itemIndex: productIndex }));
  };

  // create a POST request for ORDERS table

  return (
    <Container>
      <Snackbar
        autoHideDuration={2000}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={showSuccess}
        onClose={() => setShowSuccess(false)}
      >
        <Alert severity="success" sx={{ width: "100%" }}>
          Order submitted!
        </Alert>
      </Snackbar>

      <Snackbar
        autoHideDuration={2000}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={showError.show}
        onClose={() =>
          setShowError({
            show: false,
            message: "",
          })
        }
      >
        <Alert severity="error" sx={{ width: "100%" }}>
          {showError?.message}
        </Alert>
      </Snackbar>
      <Announcement />
      <Navbar />
      <Wrapper>
        <Title>YOUR CART</Title>
        <Top>
          <Link to={"/"} style={{ textDecoration: "none", color: "black" }}>
            <TopButton>CONTINUE SHOPPING</TopButton>
          </Link>
          <TopTexts>
            <TopText>Your Wishlist(0)</TopText>
          </TopTexts>
          {/* <TopButton type="filled">CHECKOUT NOW</TopButton> */}
        </Top>
        <Bottom>
          <Info>
            {cart.products?.map((product, index) => (
              <Product>
                <ProductDetail>
                  <DeleteForever
                    onClick={() => handleDeleteProductInCart(index)}
                    style={{ cursor: "pointer" }}
                  />
                  <Image src={product.image} />
                  <Details>
                    <ProductName>
                      <b>Product:</b> {product.title}
                    </ProductName>
                    <ProductId>
                      <b>Id:</b> {product._id}
                    </ProductId>
                    <ProductColor color={product.color} />
                    <ProductSize>
                      <b>Size:</b> {product.size}
                    </ProductSize>
                  </Details>
                </ProductDetail>
                <PriceDetail>
                  <ProductAmountContainer>
                    <Remove
                      style={{
                        cursor: "pointer",
                        color: `${product.quantity < 1 ? "gray" : "red"}`,
                      }}
                      onClick={() => {
                        if (product.quantity === 1)
                          return handleDeleteProductInCart(index);
                        reduceProductQuantity(index);
                      }}
                    />
                    <ProductAmount>{product.quantity}</ProductAmount>
                    <Add
                      style={{ cursor: "pointer" }}
                      onClick={() => {
                        if (product.quantity < 50)
                          return addProductQuantity(index);
                      }}
                    />
                  </ProductAmountContainer>
                  <ProductPrice>
                    ${product.price * product.quantity}
                  </ProductPrice>
                </PriceDetail>
              </Product>
            ))}
            <Hr />
          </Info>
          <Summary>
            <SummaryTitle>ORDER SUMMARY</SummaryTitle>
            <SummaryItem>
              <SummaryItemText>Subtotal</SummaryItemText>
              <SummaryItemPrice>${cart.total}</SummaryItemPrice>
            </SummaryItem>
            <SummaryItem>
              <SummaryItemText>Estimated Shipping</SummaryItemText>
              <SummaryItemPrice>$8</SummaryItemPrice>
            </SummaryItem>
            <SummaryItem>
              <SummaryItemText>Shipping Discount</SummaryItemText>
              <SummaryItemPrice>-$8</SummaryItemPrice>
            </SummaryItem>
            <SummaryItem type="total">
              <SummaryItemText>TOTAL AMOUNT</SummaryItemText>
              <SummaryItemPrice>${cart.total}</SummaryItemPrice>
            </SummaryItem>
            <SummaryButton onClick={() => setOpen(true)}>
              CHECKOUT NOW
            </SummaryButton>
            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              open={open}
              onClose={() => setOpen(false)}
              closeAfterTransition
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 500,
              }}
            >
              <Fade in={open}>
                <Box
                  sx={{
                    backgroundColor: "white",
                    zIndex: 10,
                    width: "80vw",
                    maxWidth: "320px",
                    position: "absolute",
                    overflowY: "auto",
                    left: "50%",
                    top: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                >
                  <Typography
                    id="transition-modal-title"
                    variant="h6"
                    component="h2"
                  >
                    <Component>
                      <Title>Sara:h Shop</Title>
                      <Payment>Where do you want to deliver your item?</Payment>
                      <Box
                        component="form"
                        sx={{
                          "& > :not(style)": { m: 1, width: "100%" },
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "center",
                          margin: "20px 30px",
                        }}
                        noValidate
                        autoComplete="off"
                      >
                        <TextField
                          id="outlined-basic"
                          label="First Name"
                          value={addOrder?.firstName}
                          variant="outlined"
                          autoComplete="off"
                          required
                          onChange={(e) =>
                            setAddOrder({
                              ...addOrder,
                              firstName: e.target.value,
                            })
                          }
                        />
                        <TextField
                          id="outlined-basic"
                          label="Last Name"
                          value={addOrder?.lastName}
                          variant="outlined"
                          autoComplete="off"
                          required
                          onChange={(e) =>
                            setAddOrder({
                              ...addOrder,
                              lastName: e.target.value,
                            })
                          }
                        />
                        <TextField
                          id="outlined-basic"
                          onChange={(e) =>
                            setAddOrder((prevState) => ({
                              ...prevState,
                              address: e.target.value,
                            }))
                          }
                          value={addOrder?.address}
                          label="Address"
                          multiline
                          variant="outlined"
                          required
                        />
                      </Box>
                      <Button
                        onClick={() => handleOrderCreate(addOrder)}
                        style={{ width: "80%" }}
                      >
                        SUBMIT
                      </Button>
                    </Component>
                  </Typography>
                </Box>
              </Fade>
            </Modal>
          </Summary>
        </Bottom>
      </Wrapper>

      <Footer />
    </Container>
  );
};

export default Cart;

const Container = styled.div``;
const Wrapper = styled.div`
  padding: 20px;
  ${mobile({ padding: "10px" })}
`;
const Title = styled.h1`
  font-weight: 300;
  text-align: center;
  margin-top: 30px;
`;
const Top = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
`;

const TopButton = styled.button`
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  border: ${(props) => props.type === "filled" && "none"};
  background-color: ${(props) =>
    props.type === "filled" ? "black" : "transparent"};
  color: ${(props) => props.type === "filled" && "white"};
`;
const TopTexts = styled.div`
  ${mobile({ display: "none" })}
`;
const TopText = styled.span`
  cursor: pointer;
  margin: 0px 10px;

  &:hover {
    text-decoration: underline;
  }
`;
const Bottom = styled.div`
  display: flex;
  justify-content: space-between;
  ${mobile({ flexDirection: "column" })}
`;

const Info = styled.div`
  flex: 3;
`;

const Hr = styled.hr`
  background-color: #eee;
  border: none;
  height: 1px;
`;

const Product = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 50px;
  ${mobile({ flexDirection: "column" })}
`;
const ProductDetail = styled.div`
  flex: 2;
  display: flex;

  align-items: center;
  ${mobile({ flexDirection: "column", alignItems: "center" })}
`;
const Image = styled.img`
  max-width: 200px;
  ${mobile({ width: "300px" })}
  margin: 0px 15px;
`;
const Details = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
const ProductName = styled.span``;
const ProductId = styled.span``;
const ProductColor = styled.span`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: ${(props) => props.color};
`;
const ProductSize = styled.span``;
const PriceDetail = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const ProductAmountContainer = styled.div`
  display: flex;
  align-items: center;
`;
const ProductAmount = styled.div`
  font-size: 24px;
  margin: 10px;
  border-style: solid;
  border-width: thin;
  padding: 2px 20px;
`;
const ProductPrice = styled.div`
  font-size: 30px;
  font-weight: 200;
`;
const Summary = styled.div`
  flex: 1;
  border: 0.5px solid lightgray;
  border-radius: 10px;
  padding: 20px;
  height: 50vh;
`;

const SummaryTitle = styled.h1`
  font-weight: 200;
`;
const SummaryItem = styled.div`
  margin: 30px 0px;
  display: flex;
  justify-content: space-between;
  font-weight: ${(props) => props.type === "total" && "500"};
  font-size: ${(props) => props.type === "total" && "24px"};
`;
const SummaryItemText = styled.span``;
const SummaryItemPrice = styled.span``;
const SummaryButton = styled.button`
  width: 100%;
  padding: 10px;
  background-color: black;
  color: white;
  font-weight: 600;
  cursor: pointer;
  margin: 5px 0px;
`;

const Component = styled.div`
  height: 50vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Payment = styled.p`
  text-align: center;
  font-size: 15px;
`;

const Button = styled.button`
  width: 100%;
  padding: 10px;
  background-color: black;
  color: white;
  font-weight: 600;
  cursor: pointer;
  margin: 20px 0px;
`;
