import React, { useState, useEffect } from "react";
import Announcement from "../components/Announcement";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import styled from "styled-components";
import { adminRequest } from "../requestMethods";
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@material-ui/core";
import {
  Skeleton,
  Button,
  TextField,
  Drawer,
  Switch,
  Snackbar,
  Alert,
  Chip,
  MenuItem,
  InputLabel,
  Select,
  OutlinedInput,
  Avatar,
  CircularProgress,
  Stack,
} from "@mui/material";
import { PhotoCamera } from "@material-ui/icons";
import Box from "@mui/material/Box";
import { useSelector } from "react-redux";
import axios from "axios";
import { mobile } from "../responsive";

const colors = [
  "All",
  "Black",
  "White",
  "Brown",
  "Beige",
  "Cream",
  "Blue",
  "Red",
  "Pink",
  "Yellow",
  "Green",
  "Orange",
];

const types = [
  "All",
  "Shoulder",
  "Tote",
  "Backpack",
  "Handbag",
  "Crossbody",
  "Sling",
  "Two-way",
  "Men",
];

const sizes = ["S", "M", "L", "XL"];

// cloudinary
// cloud name: sarahbarlis
// api key: 226857727526461
// api secret: cAzHl5fCKNbNOrHbbsbu6GkghH4
// api environment variable: CLOUDINARY_URL=cloudinary://226857727526461:cAzHl5fCKNbNOrHbbsbu6GkghH4@sarahbarlis

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const Admin = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [showEditProductDrawer, setShowEditProductDrawer] = useState(false);
  const [showAddProductDrawer, setShowAddProductDrawer] = useState(false);
  const [currentProduct, setCurrentProduct] = useState(null);
  const [addProduct, setAddProduct] = useState({
    title: "",
    description: "",
    image: "",
    cat: "Leather",
    type: ["All"],
    size: ["S"],
    color: ["All"],
    price: 0,
    inStock: false,
  });
  const [showSuccess, setShowSuccess] = useState(false);
  const [triggerRefetch, setTriggerRefetch] = useState(false);
  const [image, setImage] = useState("");
  const [imageLoading, setImageLoading] = useState(false);
  const user = useSelector((state) => state?.user?.currentUser);

  const handleColorChange = (event) => {
    const {
      target: { value },
    } = event;
    setAddProduct((prevState) => ({
      ...prevState,
      // On autofill we get a stringified value.
      color: typeof value === "string" ? value.split(",") : value,
    }));

    if (!currentProduct._id) return;
    setCurrentProduct((prevState) => ({
      ...prevState,
      color: typeof value === "string" ? value.split(",") : value,
    }));
  };

  const handleTypeChange = (event) => {
    const {
      target: { value },
    } = event;
    setAddProduct((prevState) => ({
      ...prevState,
      // On autofill we get a stringified value.
      type: typeof value === "string" ? value.split(",") : value,
    }));

    if (!currentProduct._id) return;
    setCurrentProduct((prevState) => ({
      ...prevState,
      type: typeof value === "string" ? value.split(",") : value,
    }));
  };

  const handleSizesChange = (event) => {
    const {
      target: { value },
    } = event;
    setAddProduct((prevState) => ({
      ...prevState,
      // On autofill we get a stringified value.
      size: typeof value === "string" ? value.split(",") : value,
    }));

    // guard clause
    if (!currentProduct._id) return;
    setCurrentProduct((prevState) => ({
      ...prevState,
      size: typeof value === "string" ? value.split(",") : value,
    }));
  };

  const getAllProduct = async () => {
    try {
      const res = await adminRequest.get("/products");
      setProducts(res.data);
      setLoading(false);
      setTriggerRefetch(false);
    } catch {
      setLoading(false);
    }
  };

  //get all products
  useEffect(() => {
    getAllProduct();
  }, []);

  //refetch
  useEffect(() => {
    if (!triggerRefetch) return;
    getAllProduct();
  }, [triggerRefetch]);

  // upload image to cloudinary automatically when a file is added to image state
  useEffect(() => {
    if (!image.name) return;

    handleImageUpload();
  }, [image]);

  const handleImageUpload = async () => {
    const data = new FormData();
    data.append("file", image);
    data.append("upload_preset", "products");
    data.append("cloud_name", "sarahb1");

    setImageLoading(true);

    try {
      const res = await axios.post(
        "https://api.cloudinary.com/v1_1/sarahb1/upload",
        data
      );
      setAddProduct((prevState) => ({
        ...prevState,
        image: res.data.secure_url,
      }));
      setImageLoading(false);

      if (!currentProduct._id) return;
      setCurrentProduct((prevState) => ({
        ...prevState,
        image: res.data.secure_url,
      }));
    } catch (err) {
      console.log(`Unable to upload image due to ${err}`);
    }
  };

  const allProducts = loading
    ? [...Array(5)].map((row, index) => (
        <TableRow key={index}>
          <TableCell width="20%">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell width="40%">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell width="40%">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell width="10%">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell width="10%">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell width="20%">
            <Skeleton variant="text" />
          </TableCell>
        </TableRow>
      ))
    : products?.map((product, index) => (
        <TableRow key={`${product.title}${index}`}>
          <TableCell variant="body" width="20%">
            {product.title}
          </TableCell>
          <TableCell variant="body" width="">
            <Box
              sx={{
                width: 160,
                height: 160,
                backgroundImage: `url(\'${product.image}\')`,
                backgroundSize: "75% 75%",
                backgroundPosition: "50% 50%",
                backgroundRepeat: "no-repeat",
                objectFit: "contain",
                "&:hover": {
                  opacity: [0.9, 0.8, 0.7],
                },
              }}
            />
          </TableCell>
          <TableCell
            variant="body"
            width="40%"
            style={{
              minWidth: "240px",
            }}
          >
            {product.description}
          </TableCell>
          <TableCell variant="body" width="10%">
            {product.price}
          </TableCell>
          <TableCell variant="body" width="10%">
            {product.inStock ? (
              <Chip color="success" variant="outlined" label="AVAILABLE" />
            ) : (
              <Chip color="warning" variant="outlined" label="NOT AVAILABLE" />
            )}
          </TableCell>
          <TableCell variant="body" width="20%">
            <div
              style={{
                display: "flex",
                gap: "8px",
              }}
            >
              <Button
                variant="contained"
                style={{ backgroundColor: "black" }}
                onClick={() => {
                  setShowEditProductDrawer(true);
                  setCurrentProduct(product);
                }}
              >
                UPDATE
              </Button>
              {product.inStock ? (
                <Button
                  variant="outlined"
                  color="error"
                  onClick={() => {
                    handleProductAvailabilityToggle(product, false);
                  }}
                >
                  DISABLE
                </Button>
              ) : (
                <Button
                  variant="outlined"
                  color="success"
                  onClick={() => {
                    handleProductAvailabilityToggle(product, true);
                  }}
                >
                  ENABLE
                </Button>
              )}
            </div>
          </TableCell>
        </TableRow>
      ));

  // Updating product
  const handleProductUpdate = async (product) => {
    setLoading(true);
    try {
      const res = await adminRequest.put(
        `/products/${product._id}`,
        {
          ...product,
        },
        {
          headers: {
            token: `Bearer ${user?.accessToken}`,
          },
        }
      );

      setTriggerRefetch(true);
      setShowEditProductDrawer(false);
      setShowSuccess(true);
      setLoading(false);
    } catch (err) {
      console.log(`Unable to update product due to ${err}!`);
    }
  };

  //   Add product

  const handleProductCreate = async (product) => {
    setLoading(true);
    try {
      const res = await adminRequest.post(
        `/products/`,
        {
          ...product,
        },
        {
          headers: {
            token: `Bearer ${user?.accessToken}`,
          },
        }
      );

      setShowAddProductDrawer(false);
      setTriggerRefetch(true);
      setShowSuccess(true);
      setLoading(false);
    } catch (err) {
      console.log(`Unable to add product due to ${err}!`);
    }
  };

  // Toggle part where to update/disable products

  const handleProductAvailabilityToggle = async (product, available) => {
    try {
      const res = await adminRequest.put(
        `/products/${product._id}`,
        {
          ...product,
          inStock: available,
        },
        {
          headers: {
            token: `Bearer ${user?.accessToken}`,
          },
        }
      );

      setTriggerRefetch(true);
      setShowEditProductDrawer(false);
      setShowSuccess(true);
      setCurrentProduct(null);
    } catch (err) {
      console.log(`Unable to update product due to ${err}!`);
      setCurrentProduct(null);
    }
  };

  return (
    <>
      <Snackbar
        autoHideDuration={2000}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={showSuccess}
        onClose={() => setShowSuccess(false)}
      >
        <Alert severity="success" sx={{ width: "100%" }}>
          Successfully updated the product!
        </Alert>
      </Snackbar>
      <Announcement />
      <Navbar />
      <Header>ADMIN DASHBOARD</Header>

      <ControlPanel>
        <Button
          variant="contained"
          sx={{ backgroundColor: "black" }}
          onClick={() => setShowAddProductDrawer(true)}
        >
          ADD PRODUCT
        </Button>
      </ControlPanel>

      {/* Add product form */}
      <Drawer
        open={showAddProductDrawer}
        onClose={() => {
          setShowAddProductDrawer(false);
          setAddProduct({
            title: "",
            description: "",
            image: "",
            cat: "Leather",
            type: ["All"],
            size: ["S"],
            color: ["All"],
            price: 0,
            inStock: false,
          });
        }}
        transitionDuration={150}
        anchor="right"
      >
        <DrawerContent>
          <DrawerTitle>Add a Product</DrawerTitle>

          <Box
            sx={{
              position: "relative",
              border: `${image.name ? "4px solid black" : "2px dashed grey"}`,
              borderRadius: "8px",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "100%",
              height: "auto",
              aspectRatio: "1/1",
              backgroundColor: "grey",
              backgroundImage: `url('${addProduct.image}')`,
              objectFit: "cover",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              marginBottom: "32px",
            }}
          >
            {imageLoading ? (
              <CircularProgress color="secondary" />
            ) : (
              <Avatar variant="rounded">
                <PhotoCamera />
              </Avatar>
            )}
            <input
              hidden
              accept="image/*"
              multiple
              type="file"
              onChange={(e) => {
                setImage(e.target.files[0]);
                console.log("files", e.target.files[0]);
              }}
              style={{
                position: "absolute",
                display: "flex",
                width: "100%",
                height: "100%",
                cursor: "pointer",
                opacity: 0,
              }}
            />
          </Box>

          <StyledTextField
            id="outlined-basic"
            label="Name:"
            variant="outlined"
            style={{ marginBottom: "24px" }}
            value={addProduct?.title}
            fullWidth
            onChange={(e) =>
              setAddProduct((prevState) => ({
                ...prevState,
                title: e.target.value,
              }))
            }
          />
          <StyledTextField
            id="outlined-basic"
            label="Description:"
            variant="outlined"
            multiline
            style={{ marginBottom: "24px" }}
            value={addProduct?.description}
            fullWidth
            onChange={(e) =>
              setAddProduct((prevState) => ({
                ...prevState,
                description: e.target.value,
              }))
            }
          />
          <StyledTextField
            id="outlined-basic"
            label="Price:"
            variant="outlined"
            style={{ marginBottom: "24px" }}
            value={addProduct?.price}
            fullWidth
            onChange={(e) =>
              setAddProduct((prevState) => ({
                ...prevState,
                price: e.target.value,
              }))
            }
            type="number"
          />

          <Stack
            direction="row"
            spacing={2}
            sx={{
              marginBottom: "16px",
            }}
          >
            {/* Selection for colors */}
            <FormControl sx={{ m: 1, width: 300 }}>
              <InputLabel>Colors</InputLabel>
              <Select
                multiple
                value={addProduct.color}
                onChange={handleColorChange}
                input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
                renderValue={(selected) => (
                  <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                    {selected.map((value) => (
                      <Chip key={value} label={value} />
                    ))}
                  </Box>
                )}
                MenuProps={MenuProps}
              >
                {colors.map((color) => (
                  <MenuItem key={color} value={color}>
                    {color}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            {/* Selection for types */}
            <FormControl sx={{ m: 1, width: 300 }}>
              <InputLabel>Type</InputLabel>
              <Select
                multiple
                value={addProduct.type}
                onChange={handleTypeChange}
                input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
                renderValue={(selected) => (
                  <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                    {selected.map((value) => (
                      <Chip key={value} label={value} />
                    ))}
                  </Box>
                )}
                MenuProps={MenuProps}
              >
                {types.map((type) => (
                  <MenuItem key={type} value={type}>
                    {type}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            {/* Selection for sizes */}
            <FormControl sx={{ m: 1, width: 300 }}>
              <InputLabel>Sizes</InputLabel>
              <Select
                multiple
                value={addProduct.size}
                onChange={handleSizesChange}
                input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
                renderValue={(selected) => (
                  <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                    {selected.map((value) => (
                      <Chip key={value} label={value} />
                    ))}
                  </Box>
                )}
                MenuProps={MenuProps}
              >
                {sizes.map((size) => (
                  <MenuItem key={size} value={size}>
                    {size}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Stack>

          {/* Selection for Category */}
          <FormControl
            fullWidth
            style={{
              marginBottom: "16px",
            }}
          >
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={addProduct.cat}
              label="Category"
              onChange={(e) =>
                setAddProduct((prevState) => ({
                  ...prevState,
                  cat: e.target.value,
                }))
              }
            >
              <MenuItem value="Leather">Leather</MenuItem>
              <MenuItem value="Nylon">Nylon</MenuItem>
              <MenuItem value="Fabric">Fabric</MenuItem>
            </Select>
          </FormControl>

          {/* Switch */}
          <div style={{ marginBottom: "48px" }}>
            Available:{" "}
            <Switch
              checked={addProduct?.inStock}
              size="medium"
              onChange={(e) =>
                setAddProduct((prevState) => ({
                  ...prevState,
                  inStock: e.target.checked,
                }))
              }
            />
          </div>

          <DrawerActions>
            <Button
              variant="contained"
              style={{ marginRight: "16px", backgroundColor: "black" }}
              onClick={() => handleProductCreate(addProduct)}
            >
              SAVE
            </Button>
            <Button
              variant="outlined"
              color="error"
              onClick={() => {
                setShowAddProductDrawer(false);
              }}
            >
              CANCEL
            </Button>
          </DrawerActions>
        </DrawerContent>
      </Drawer>

      <Drawer
        open={showEditProductDrawer}
        onClose={() => {
          setShowEditProductDrawer(false);
          setCurrentProduct(null);
        }}
        transitionDuration={150}
        anchor="right"
      >
        <DrawerContent>
          <DrawerTitle>Update Product</DrawerTitle>

          <Box
            sx={{
              position: "relative",
              border: `${
                currentProduct?.image ? "4px solid black" : "2px dashed grey"
              }`,
              borderRadius: "8px",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "100%",
              height: "auto",
              aspectRatio: "1/1",
              backgroundColor: "grey",
              backgroundImage: `url('${currentProduct?.image}')`,
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              marginBottom: "32px",
            }}
          >
            {imageLoading ? (
              <CircularProgress color="secondary" />
            ) : (
              <Avatar variant="rounded">
                <PhotoCamera />
              </Avatar>
            )}
            <input
              hidden
              accept="image/*"
              multiple
              type="file"
              onChange={(e) => {
                setImage(e.target.files[0]);
              }}
              style={{
                position: "absolute",
                display: "flex",
                width: "100%",
                height: "100%",
                cursor: "pointer",
                opacity: 0,
              }}
            />
          </Box>

          <StyledTextField
            id="outlined-basic"
            label="Name:"
            variant="outlined"
            style={{ marginBottom: "24px" }}
            value={currentProduct?.title}
            fullWidth
            onChange={(e) =>
              setCurrentProduct((prevState) => ({
                ...prevState,
                title: e.target.value,
              }))
            }
          />
          <StyledTextField
            id="outlined-basic"
            label="Description:"
            variant="outlined"
            multiline
            style={{ marginBottom: "24px" }}
            value={currentProduct?.description}
            fullWidth
            onChange={(e) =>
              setCurrentProduct((prevState) => ({
                ...prevState,
                description: e.target.value,
              }))
            }
          />
          <StyledTextField
            id="outlined-basic"
            label="Price:"
            variant="outlined"
            style={{ marginBottom: "24px" }}
            value={currentProduct?.price}
            fullWidth
            onChange={(e) =>
              setCurrentProduct((prevState) => ({
                ...prevState,
                price: e.target.value,
              }))
            }
            type="number"
          />

          <Stack
            direction="row"
            spacing={2}
            sx={{
              marginBottom: "16px",
            }}
          >
            {/* Selection for colors */}
            <FormControl sx={{ m: 1, width: 300 }}>
              <InputLabel>Colors</InputLabel>
              <Select
                multiple
                value={currentProduct?.color || []}
                onChange={handleColorChange}
                input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
                renderValue={(selected) => (
                  <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                    {selected.map((value) => (
                      <Chip key={value} label={value} />
                    ))}
                  </Box>
                )}
                MenuProps={MenuProps}
              >
                {colors.map((color) => (
                  <MenuItem key={color} value={color}>
                    {color}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            {/* Selection for types */}
            <FormControl sx={{ m: 1, width: 300 }}>
              <InputLabel>Type</InputLabel>
              <Select
                multiple
                value={currentProduct?.type || []}
                onChange={handleTypeChange}
                input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
                renderValue={(selected) => (
                  <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                    {selected.map((value) => (
                      <Chip key={value} label={value} />
                    ))}
                  </Box>
                )}
                MenuProps={MenuProps}
              >
                {types.map((type) => (
                  <MenuItem key={type} value={type}>
                    {type}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            {/* Selection for sizes */}
            <FormControl sx={{ m: 1, width: 300 }}>
              <InputLabel>Sizes</InputLabel>
              <Select
                multiple
                value={currentProduct?.size || []}
                onChange={handleSizesChange}
                input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
                renderValue={(selected) => (
                  <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                    {selected.map((value) => (
                      <Chip key={value} label={value} />
                    ))}
                  </Box>
                )}
                MenuProps={MenuProps}
              >
                {sizes.map((size) => (
                  <MenuItem key={size} value={size}>
                    {size}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Stack>

          {/* Selection for Category */}
          <FormControl
            fullWidth
            style={{
              marginBottom: "16px",
            }}
          >
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={currentProduct?.cat}
              label="Category"
              onChange={(e) =>
                setCurrentProduct((prevState) => ({
                  ...prevState,
                  cat: e.target.value,
                }))
              }
            >
              <MenuItem value="Leather">Leather</MenuItem>
              <MenuItem value="Nylon">Nylon</MenuItem>
              <MenuItem value="Fabric">Fabric</MenuItem>
            </Select>
          </FormControl>

          <div style={{ marginBottom: "48px" }}>
            Available:{" "}
            <Switch
              checked={currentProduct?.inStock}
              size="medium"
              onChange={(e) =>
                setCurrentProduct((prevState) => ({
                  ...prevState,
                  inStock: e.target.checked,
                }))
              }
            />
          </div>

          <DrawerActions>
            <Button
              variant="contained"
              style={{ marginRight: "16px", backgroundColor: "black" }}
              onClick={() => handleProductUpdate(currentProduct)}
            >
              SAVE
            </Button>
            <Button
              variant="outlined"
              color="error"
              onClick={() => {
                setCurrentProduct(null);
                setShowEditProductDrawer(false);
              }}
            >
              CANCEL
            </Button>
          </DrawerActions>
        </DrawerContent>
      </Drawer>

      <Container>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell variant="head">NAME</TableCell>
              <TableCell variant="head">IMAGE</TableCell>
              <TableCell variant="head">DESCRIPTION</TableCell>
              <TableCell variant="head">PRICE</TableCell>
              <TableCell variant="head">AVAILABILITY</TableCell>
              <TableCell variant="head">ACTIONS</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{allProducts}</TableBody>
        </Table>
      </Container>

      <Footer />
    </>
  );
};

const Container = styled.div`
  display: flex;
  padding: 16px 5vw 0 5vw;
  overflow: auto;

  margin-bottom: 40px;
`;

const ControlPanel = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 0 5vw;

  width: 100%;
  margin-bottom: 16px;
`;

const DrawerContent = styled.div`
  padding: 48px 48px 48px 24px;
  display: flex;
  flex-flow: column;
  justify-content: flex-end;
  width: 50vw;
  max-width: 320px;
`;

const DrawerTitle = styled.div`
  margin-bottom: 24px;

  font-size: 32px;
  font-weight: 600;
  text-transform: uppercase;

  ${mobile({ fontSize: "30px" })}
`;

const DrawerActions = styled.div`
  display: flex;
  justify-content: space-between;
`;

const StyledTextField = styled(TextField)`
  display: flex;
`;

const Header = styled.h1`
  margin: 20px 0px;
  text-align: center;
`;

const FormControl = styled.div``;

export default Admin;
