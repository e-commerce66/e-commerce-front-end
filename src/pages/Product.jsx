import styled from "styled-components";
import Announcement from "../components/Announcement";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import NewsLetter from "../components/NewsLetter";
import { Remove } from "@material-ui/icons";
import { Add } from "@material-ui/icons";
import { mobile } from "../responsive";
import { useLocation, Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { publicRequest } from "../requestMethods";
import { addProduct } from "../redux/cartRedux";
import { useDispatch, useSelector } from "react-redux";
import { Stack, Rating } from "@mui/material/";

const Product = () => {
  const location = useLocation();
  const id = location.pathname.split("/")[2];
  const [product, setProduct] = useState({});
  const [color, setColor] = useState("");
  const [size, setSize] = useState("");
  const [quantity, setQuantity] = useState(1);
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.currentUser);

  // Fetching product in database
  useEffect(() => {
    const getProduct = async () => {
      try {
        const res = await publicRequest.get("/products/find/" + id);
        setProduct(res.data);

        setSize(res.data.size[0]);
        setColor(res.data.color[1] || res.data.color[0]);
      } catch {}
    };
    getProduct();
  }, [id]);

  // Adding and removing quantity
  const handleQuantity = (type) => {
    if (type === "dec") {
      quantity > 1 && setQuantity(quantity - 1);
    } else {
      setQuantity(quantity + 1);
    }
  };

  // Adding to cart
  const handleClick = () => {
    dispatch(addProduct({ ...product, quantity, color, size }));
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Container>
      <Announcement />
      <Navbar />
      <Wrapper>
        <ImageContainer>
          <Image src={product.image} />
        </ImageContainer>
        <InfoContainer>
          <Title>{product.title}</Title>
          <Stack spacing={2} sx={{ marginBottom: "20px" }}>
            <Rating name="size-medium" defaultValue={5} />
          </Stack>
          <Price>${product.price}</Price>
          <Description>{product.description}</Description>
          <FilterContainer>
            <Filter>
              <FilterTitle>Color</FilterTitle>
              {product.color?.slice(1).map((c) => (
                <FilterColor color={c} key={c} onClick={() => setColor(c)} />
              ))}
            </Filter>
            <Filter>
              <FilterTitle>Size</FilterTitle>
              <FilterSize onChange={(e) => setSize(e.target.value)}>
                {product.size?.map((s) => (
                  <FilterSizeOption key={s}>{s}</FilterSizeOption>
                ))}
              </FilterSize>
            </Filter>
          </FilterContainer>
          <AddContainer>
            <AmountContainer>
              <Remove onClick={() => handleQuantity("dec")} />
              <Amount>{quantity}</Amount>
              <Add onClick={() => handleQuantity("inc")} />
            </AmountContainer>
            {user && user.isAdmin ? (
              <Button
                as={Link}
                to="/logout"
                style={{ textDecoration: "none", color: "white" }}
              >
                LOGIN AS USER
              </Button>
            ) : user && user._id ? (
              <Button onClick={handleClick}>ADD TO CART</Button>
            ) : (
              <Button
                as={Link}
                to="/login"
                style={{ textDecoration: "none", color: "white" }}
              >
                LOGIN AS USER
              </Button>
            )}
          </AddContainer>
        </InfoContainer>
      </Wrapper>
      <NewsLetter />
      <Footer />
    </Container>
  );
};

export default Product;

const Container = styled.div``;
const Wrapper = styled.div`
  padding: 50px;
  display: flex;
  ${mobile({ flexDirection: "column", padding: "10px", marginBottom: "50px" })}
`;
const ImageContainer = styled.div`
  flex: 1;
  margin-left: 80px;
`;
const InfoContainer = styled.div`
  flex: 1;
  padding: 0 50px;
  ${mobile({ padding: "0 20px" })}
`;
const Image = styled.img`
  width: 80%;
  height: 70vh;
  object-fit: cover;
  ${mobile({ height: "40vh" })}
`;
const Title = styled.h1`
  font-weight: 400;
`;
const Price = styled.span`
  font-size: 20px;
`;

const Description = styled.p`
  margin-top: 10px;
  font-weight: 380;
  letter-spacing: 1px;
`;
const FilterContainer = styled.div`
  width: 50%;
  margin: 70px 0;
  display: flex;
  justify-content: space-between;
  ${mobile({ width: "90%" })}
`;
const Filter = styled.div`
  display: flex;
  align-items: center;
`;
const FilterTitle = styled.span`
  font-size: 20px;
  font-weight: 400;
  margin-right: 10px;
`;
const FilterColor = styled.div`
  width: 20px;
  height: 20px;
  background-color: ${(props) => props.color};
  border: 1px solid;
  margin-right: 7px;
  cursor: pointer;
`;
const FilterSize = styled.select`
  padding: 4px;
`;
const FilterSizeOption = styled.option``;

const AddContainer = styled.div`
  width: 50%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  ${mobile({ width: "90%" })}
`;
const AmountContainer = styled.div`
  display: flex;
  align-items: center;
  font-weight: 600;
`;
const Amount = styled.span`
  width: 30px;
  height: 30px;
  border: 0.5px solid;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0px 5px;
`;
const Button = styled.button`
  padding: 15px;
  border: none;
  background-color: #3949ab;
  cursor: pointer;
  color: white;
  border-radius: 3px;

  &:hover {
    background-color: #4c5cba;
  }
`;
