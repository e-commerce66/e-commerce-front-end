import styled from "styled-components";
import Announcement from "../components/Announcement";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import { mobile } from "../responsive";

const Container = styled.div``;

const Wrapper = styled.div`
  height: 50vh;
`;
const ErrorWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const ErrorTitle = styled.h1`
  margin-top: 2em;
  ${mobile({ fontSize: "1.5em" })}
`;
const ErrorMessage = styled.p`
  margin-top: 1em;
`;

const ErrorPage = () => {
  return (
    <Container>
      <Announcement />
      <Navbar />
      <Wrapper>
        <ErrorWrapper>
          <ErrorTitle>Error: 404 Page Not Found</ErrorTitle>
          <ErrorMessage>
            Go back to <a href="/">homepage.</a>
          </ErrorMessage>
        </ErrorWrapper>
      </Wrapper>
      <Footer />
    </Container>
  );
};

export default ErrorPage;
