import React from "react";
import { useEffect } from "react";
import Announcement from "../components/Announcement";
import { Categories } from "../components/Categories";
import Navbar from "../components/Navbar";
import NewsLetter from "../components/NewsLetter";
import PopularProducts from "../components/PopularProducts";
import Slider from "../components/Slider";
import Footer from "../components/Footer";
import styled from "styled-components";
import { mobile } from "../responsive";

const Header = styled.h1`
  margin: 20px 0px;
  text-align: center;
  ${mobile({ fontSize: "20px" })}
`;

const Home = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div>
      <Announcement />
      <Navbar />
      <Slider />
      <Categories />
      <Header>POPULAR PRODUCTS</Header>
      <PopularProducts />
      <NewsLetter />
      <Footer />
    </div>
  );
};

export default Home;
