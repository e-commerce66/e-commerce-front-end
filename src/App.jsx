import { BrowserRouter as Router } from "react-router-dom";
import { Routes, Route, Navigate } from "react-router-dom";
import Home from "./pages/Home";
import ProductList from "./pages/ProductList";
import Product from "./pages/Product";
import Cart from "./pages/Cart";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import ErrorPage from "./pages/ErrorPage";
import Success from "./pages/Success";
import Admin from "./pages/Admin";
import { useSelector } from "react-redux";
import OrderHistory from "./pages/OrderHistory";
import Search from "./pages/Search";
import styled from "styled-components";
import AdminAllOrders from "./pages/AdminAllOrders";

// import Form from "./components/Form";
// import Navbar from "./components/Navbar";
// import Announcement from "./components/Navbar";

const App = () => {
  const user = useSelector((state) => state.user.currentUser);

  return (
    <Container>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/orders" element={<OrderHistory />} />
          <Route path="/search" element={<Search />} />
          <Route path="/product/:id" element={<Product />} />
          <Route path="/products/:category" element={<ProductList />} />
          <Route
            path="/login"
            element={user ? <Navigate to="/" /> : <Login />}
          />
          <Route
            path="/register"
            element={user ? <Navigate to="/" /> : <Register />}
          />
          <Route path="/logout" element={<Logout />} />
          <Route path="*" element={<ErrorPage />} />
          {/* If the checkout is successful */}
          <Route path="/success" element={<Success />} />
          <Route path="/adminorders" element={<AdminAllOrders />} />
          {user?.isAdmin && <Route path="/admin" element={<Admin />} />}
        </Routes>
      </Router>
    </Container>
  );
};

const Container = styled.div`
  * {
    box-sizing: border-box;
  }
`;

export default App;
